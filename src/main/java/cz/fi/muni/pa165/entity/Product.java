package cz.fi.muni.pa165.entity;

import cz.fi.muni.pa165.enums.Color;
import cz.fi.muni.pa165.validation.AllOrNothing;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.*;

@Entity
@AllOrNothing(members = {"image", "imageMimeType"})
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Lob
    private byte[] image;

    private String imageMimeType;

    @NotNull
    private String name;

    private LocalDate addedDate;

    @OneToOne
    @JoinTable(name = "CURRENT_PRICE")
    private Price currentPrice;

    @OneToMany()
    @OrderBy("priceStart DESC")
    @JoinColumn(name = "Product_FK")
    private List<Price> priceHistory = new ArrayList<>();

    @Enumerated
    private Color color;

    @ManyToMany(mappedBy = "products")
    private Set<Category> categories = new HashSet<>();

    public Product(Long productId) {
        this.id = productId;
    }

    public Product() {
    }

    public void setId(Long id) {
        this.id = id;
    }


    public void removeCategory(Category category) {
        this.categories.remove(category);
    }

    public void addCategory(Category c) {
        categories.add(c);
        c.addProduct(this);
    }

    public Set<Category> getCategories() {
        return Collections.unmodifiableSet(categories);
    }

    public LocalDate getAddedDate() {
        return addedDate;
    }

    public void setAddedDate(LocalDate addedDate) {
        this.addedDate = addedDate;
    }

    public byte[] getImage() {
        return image;
    }


    public String getImageMimeType() {
        return imageMimeType;
    }


    public void setImageMimeType(String imageMimeType) {
        this.imageMimeType = imageMimeType;
    }


    public Price getCurrentPrice() {
        return currentPrice;
    }


    public void addHistoricalPrice(Price p) {
        priceHistory.add(p);
    }

    public void setCurrentPrice(Price currentPrice) {
        this.currentPrice = currentPrice;
    }

    public List<Price> getPriceHistory() {
        return Collections.unmodifiableList(priceHistory);
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public Long getId() {
        return id;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof Product))
            return false;
        Product other = (Product) obj;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.getName()))
            return false;
        return true;
    }
}
