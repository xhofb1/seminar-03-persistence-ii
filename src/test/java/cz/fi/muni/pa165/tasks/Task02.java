package cz.fi.muni.pa165.tasks;

import cz.fi.muni.pa165.PersistenceSampleApplicationContext;
import cz.fi.muni.pa165.entity.Category;
import cz.fi.muni.pa165.entity.Product;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.validation.ConstraintViolationException;
import java.util.Set;


@ContextConfiguration(classes = PersistenceSampleApplicationContext.class)
public class Task02 extends AbstractTestNGSpringContextTests {

    @PersistenceUnit
    private EntityManagerFactory emf;
    Category electro = new Category();
    Category kitchen = new Category();
    Product flashlight = new Product();
    Product robot = new Product();
    Product plate = new Product();
    Product empty = new Product();


    @BeforeClass
    public void setup() {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();

        electro.setName("Electro");
        kitchen.setName("Kitchen");

        em.persist(electro);
        em.persist(kitchen);


        flashlight.setName("Flashlight");
        flashlight.addCategory(electro);
        robot.setName("Kitchen robot");
        robot.addCategory(kitchen);
        robot.addCategory(electro);
        plate.setName("Plate");
        plate.addCategory(kitchen);

        em.persist(flashlight);
        em.persist(robot);
        em.persist(plate);

        em.getTransaction().commit();
        em.close();
    }

    private void assertContainsCategoryWithName(Set<Category> categories, String expectedCategoryName) {
        for (Category cat : categories) {
            if (cat.getName().equals(expectedCategoryName))
                return;
        }

        Assert.fail("Couldn't find category " + expectedCategoryName + " in collection " + categories);
    }

    private void assertContainsProductWithName(Set<Product> products, String expectedProductName) {
        for (Product prod : products) {
            if (prod.getName().equals(expectedProductName))
                return;
        }

        Assert.fail("Couldn't find product " + expectedProductName + " in collection " + products);
    }

    @Test
    public void plateTest() {
        EntityManager em = null;
        try {
            em = emf.createEntityManager();
            assertContainsCategoryWithName(plate.getCategories(), kitchen.getName());
        } finally {
            if (em != null) em.close();
        }
    }

    @Test
    public void flashlightTest() {
        EntityManager em = null;
        try {
            em = emf.createEntityManager();
            assertContainsCategoryWithName(flashlight.getCategories(), electro.getName());
        } finally {
            if (em != null) em.close();
        }
    }

    @Test
    public void robotTest() {
        EntityManager em = null;
        try {
            em = emf.createEntityManager();
            assertContainsCategoryWithName(robot.getCategories(), electro.getName());
            assertContainsCategoryWithName(robot.getCategories(), kitchen.getName());
        } finally {
            if (em != null) em.close();
        }
    }

    @Test
    public void kitchenTest() {
        EntityManager em = null;
        try {
            em = emf.createEntityManager();
            assertContainsProductWithName(kitchen.getProducts(), plate.getName());
            assertContainsProductWithName(kitchen.getProducts(), robot.getName());
        } finally {
            if (em != null) em.close();
        }
    }

    @Test
    public void electroTest() {
        EntityManager em = null;
        try {
            em = emf.createEntityManager();
            assertContainsProductWithName(electro.getProducts(), flashlight.getName());
            assertContainsProductWithName(electro.getProducts(), robot.getName());
        } finally {
            if (em != null) em.close();
        }
    }

    @Test(expectedExceptions = ConstraintViolationException.class)
    public void testDoesntSaveNullName() {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(empty);
        em.getTransaction().commit();

    }

    @Test(expectedExceptions = ConstraintViolationException.class)
    public void testAllOrNothing() {
        EntityManager em = null;
        try {
            em = emf.createEntityManager();
            em.getTransaction().begin();

            Product product = new Product();
            product.setImageMimeType("image/png");
            em.persist(product);

            em.getTransaction().commit();
        } finally {
            if (em != null) em.close();
        }
    }

}
